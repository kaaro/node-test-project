var Twitter = require('twitter');

var neo4j = require('neo4j-driver').v1;

var client = new Twitter({
  consumer_key: "JvwIzVGNfbOE1Ggzcb8wBGy3Z",
  consumer_secret:  "7wCdFLjOtVfD50dopbu5ZdkH81QC88ai6SH4SrksjthdYPtK5p",
  access_token_key:  "500175852-2HRmG2hoLcPFlHcfyixsqvGAUu2Y3g6tYnhkdaZA",
  access_token_secret: "2B79d5ZwRhpN6Y01iVvd21KmRKBcTKmxDthSSXRJgRWIp"
});

var params = {screen_name: 'vidhant'};
client.get('statuses/user_timeline', params, function(error, tweets, response) {
  if (!error) {
    // console.log(tweets);
    for( var tweet in tweets) {
        forEachTweet(tweets[tweet]);
    }
  }
});
client.get('followers/list', params, function(error, follower_list, response) {
    if (!error) {
    //   console.log(follower_list);
      for( var user in follower_list.users) {
          forEachUser(follower_list.users[user]);
      }
    }
  });

var driver = neo4j.driver("bolt://localhost", neo4j.auth.basic("neo4j", "test"));
var session = driver.session();

// Run a Cypher statement, reading the result in a streaming manner as records arrive:

function forEachTweet(tweet) {
  
    var builderString = "";
    builderString += "\n\n\n " +
        "\nMERGE (tweet:Tweet " +
        "\n        {  " +
        "\n            created_at: '" + tweet.created_at + "'," + 
        "\n            id: " + tweet.id + "," +
        "\n            text: '" + tweet.text.replace("'"," ") + "'" +
        "\n        }" +
        "\n    )" +
        "\n";
    builderString += "" + 
        "\nMERGE (user:User " +
        "\n        {" +
        "\n            id:" + tweet.user.id + ", " +
        "\n            name: '" + tweet.user.name + "', " +
        "\n            screen_name: '" + tweet.user.screen_name + "' " +
        "\n        }" +
        "\n    )" +
        "\n";
    if(tweet.entities.user_mentions.length > 0)
        builderString += "" +
            "\nMERGE (mention_user:User " +
            "\n        {" +
            "\n            id:" + tweet.entities.user_mentions[0].id + ", " +
            "\n            name: '" + tweet.entities.user_mentions[0].name + "', " +
            "\n            screen_name: '" + tweet.entities.user_mentions[0].screen_name + "' " +
            "\n        }" +
            "\n    )" +
            "\n";
    builderString += "" +
        "\nMerge (tweet) <-[:Tweeted]- (user)" +
            "\n";
    if(tweet.entities.user_mentions.length > 0)
        builderString += "" +
        "\nMerge (tweet) -[:Mentions]-> (mention_user)" +
            "\n";
    builderString += "\nreturn tweet, user";
    if(tweet.entities.user_mentions.length > 0)
        builderString += ", mention_user";
    console.log(builderString);
    
    session
        .run(builderString)
        .then(function (result) {
            // console.log(builderString);
            // result.records.forEach(function (record) {
            // console.log(record.get('name'));
            // });
            session.close();
        })
        .catch(function (error) {
            console.log(builderString);
            console.log(error);
        });
}


function forEachUser(user) {
    var builderString = "";
    builderString += "\n\n\n " +
        "\nMERGE (user:User " +
        "\n        {  " +
        "\n            screen_name: '" + params.screen_name + "'" + 
        "\n        }" +
        "\n    )" +
        "\n";
    builderString += "" + 
        "\nMERGE (follower:User " +
        "\n        {" +
        "\n            id:" + user.id + ", " +
        "\n            name: '" + user.name + "', " +
        "\n            screen_name: '" + user.screen_name + "' " +
        "\n        }" +
        "\n    )" +
        "\n";
    builderString += "" +
        "\nMerge (user) <-[:Follows]- (follower)" +
            "\n";
    builderString += "\nreturn user, follower";
    // console.log(builderString);
      
    session
        .run(builderString)
        .then(function (result) {
            // console.log(builderString);
            // result.records.forEach(function (record) {
            // console.log(record.get('name'));
            // });
            session.close();
        })
        .catch(function (error) {
            console.log(builderString);
            console.log(error);
        });
}
//Things that We need from a tweet
/*
    Merged_at: 'Fri Aug 31 13:44:33 +0000 2018',
    id: 1035523740153466900,
    id_str: '1035523740153466880',
    text: 'RT @karx_brb: Lets see if this works now #kaaroTweet',
    entities:
    { 
        hashtags:[
            { text: 'javascript', indices: [Array] },
            { text: 'AngularJS', indices: [Array] },
            { text: 'Angular', indices: [Array] },
            { text: 'angular2', indices: [Array] },
            { text: 'angular5', indices: [Array] },
            { text: 'VueJs', indices: [Array] } 
        ],
        symbols: [],
        user_mentions:[
            { screen_name: 'JavaScript',
            name: 'JavaScript',
            id: 539345368,
            id_str: '539345368',
            indices: [Array] } ],
        urls: [] 
    },
    user:
    { id: 500175852,
      id_str: '500175852',
      name: 'Kartik Arora',
      screen_name: 'karx_brb',
      location: 'India',
      url: 'https://t.co/YDSdWIdLox',
      followers_count: 30,
      friends_count: 145,

    }
    lang: 'en' 
    retweet_count: 3,
    */

// Close the driver when application exits.
// This closes all used network connections.
driver.close();
